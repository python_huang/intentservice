package com.example.intentservice

import android.content.Intent
import android.app.IntentService
import android.util.Log
import android.widget.Toast

// 建構子
class HelloIntentService : IntentService("HelloIntentService") {

    override fun onHandleIntent(intent: Intent?) {
        // 要做的事情寫在這裡，如下載檔案等等
        // demo使用所以我們讓它執行後印出Log，沉睡5秒再印出Log
        Log.d("HelloIntentService", "onHandleIntent start")
        val endTime = System.currentTimeMillis() + 5 * 1000
        while (System.currentTimeMillis() < endTime) {
            synchronized(this) {
                try {
                    Thread.sleep(endTime - System.currentTimeMillis())
                } catch (e: Exception) {
                }

            }
        }
        Log.d("HelloIntentService", "onHandleIntent finish")
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "Finish", Toast.LENGTH_SHORT).show()
    }

}
